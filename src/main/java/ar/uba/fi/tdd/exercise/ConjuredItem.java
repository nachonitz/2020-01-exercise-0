package ar.uba.fi.tdd.exercise;

public class ConjuredItem extends GeneralItem{

    public void updateQuality(Item item){
        if (dateExpired(item)){
            item.quality -= 4;
        }
        else{
            item.quality -= 2;
        }
        if (negativeQuality(item)){
            item.quality = 0;
        }
    }
}
