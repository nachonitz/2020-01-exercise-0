package ar.uba.fi.tdd.exercise;

public class NormalItem extends GeneralItem{

    public void updateQuality(Item item){
        if (dateExpired(item)){
            item.quality -= 2;
        }
        else{
            item.quality -= 1;
        }
        if (negativeQuality(item)){
            item.quality = 0;
        }
    }
}
