package ar.uba.fi.tdd.exercise;

public class AgedBrieItem extends GeneralItem{

    public void updateQuality(Item item){
        if (dateExpired(item)){
            item.quality += 2;
        }
        else{
            item.quality += 1;
        }
        if (maxQuality(item)){
            item.quality = 50;
        }
    }

}
