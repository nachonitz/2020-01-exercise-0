
package ar.uba.fi.tdd.exercise;

class GildedRose {
  Item[] items;
  ItemManager itemManager;

    public GildedRose(Item[] _items){
        items = _items;
        itemManager = new ItemManager();
    }

    // update the quality of the emements
    public void updateQuality() {
        // for each item
        for (int i = 0; i < items.length; i++) {
            itemManager.updateQuality(items[i]);
        }
    }
}
