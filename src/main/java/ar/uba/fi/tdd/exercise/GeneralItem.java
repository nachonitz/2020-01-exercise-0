package ar.uba.fi.tdd.exercise;

public abstract class GeneralItem {

    protected boolean dateExpired(Item item){
        return item.sellIn <= 0;
    }

    protected boolean negativeQuality(Item item){
        return item.quality < 0;
    }

    protected boolean maxQuality(Item item){
        return item.quality >= 50;
    }

    abstract void updateQuality(Item item);
}
