package ar.uba.fi.tdd.exercise;

public class BackstageItem extends GeneralItem{

    private boolean tenDaysOrLess(Item item){
        return item.sellIn <= 10;
    }
    private boolean fiveDaysOrLess(Item item){
        return item.sellIn <= 5;
    }

    public void updateQuality(Item item){
        if (fiveDaysOrLess(item)){
            item.quality += 3;
        }else{
            if (tenDaysOrLess(item)){
                item.quality += 2;
            }else{
                item.quality += 1;
            }
        }
        if (maxQuality(item)){
            item.quality = 50;
        }
        if (dateExpired(item)){
            item.quality = 0;
        }
    }
}
