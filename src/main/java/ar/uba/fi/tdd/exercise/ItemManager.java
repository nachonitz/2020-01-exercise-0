package ar.uba.fi.tdd.exercise;

public class ItemManager {
    private NormalItem normalItem;
    private AgedBrieItem agedBrieItem;
    private BackstageItem backstageItem;
    private ConjuredItem conjuredItem;

    public ItemManager(){
        this.normalItem = new NormalItem();
        this.agedBrieItem = new AgedBrieItem();
        this.backstageItem = new BackstageItem();
        this.conjuredItem = new ConjuredItem();
    }

    public void updateQuality(Item item){
        switch (item.Name){
            case "Aged Brie":
                agedBrieItem.updateQuality(item);
                break;
            case "Backstage passes to a TAFKAL80ETC concert":
                backstageItem.updateQuality(item);
                break;
            case "Sulfuras, Hand of Ragnaros":
                break;
            case "Conjured":
                conjuredItem.updateQuality(item);
                break;
            default:
                normalItem.updateQuality(item);
                break;
        }
    }
}
