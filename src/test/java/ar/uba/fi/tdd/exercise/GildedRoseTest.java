package ar.uba.fi.tdd.exercise;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class GildedRoseTest {

	@Test
	public void itemAgedBrieIncreasesQualityAfterUpdate() {
			Item[] items = new Item[] { new Item("Aged Brie", 5, 5) };
			GildedRose app = new GildedRose(items);
			app.updateQuality();
			assertThat(6).isEqualTo(app.items[0].quality);
	}

	@Test
	public void itemAgedBrieIncreasesTwiceQualityInSellin0() {
		Item[] items = new Item[] { new Item("Aged Brie", 0, 5) };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertThat(7).isEqualTo(app.items[0].quality);
	}

	@Test
	public void itemAgedBrieQualityNotMoreThan50() {
		Item[] items = new Item[] { new Item("Aged Brie", 5, 50) };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertThat(50).isEqualTo(app.items[0].quality);
	}

	@Test
	public void itemBackstagePassesIncreasesQualityBy3AfterUpdateAndSellinLessThan5() {
		Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 4, 5) };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertThat(8).isEqualTo(app.items[0].quality);
	}

	@Test
	public void itemBackstagePassesIncreasesQualityBy2AfterUpdateAndSellinLessThan10() {
		Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 7, 5) };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertThat(7).isEqualTo(app.items[0].quality);
	}

	@Test
	public void itemBackstagePassesDropsQualityTo0InSellin0() {
		Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 0, 10) };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertThat(0).isEqualTo(app.items[0].quality);
	}

	@Test
	public void itemBackstagePassesQualityNotMoreThan50() {
		Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 2, 49) };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertThat(50).isEqualTo(app.items[0].quality);
	}

	@Test
	public void itemSulfurasSameQuality() {
		Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", 8, 10) };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertThat(10).isEqualTo(app.items[0].quality);
	}

	@Test
	public void itemSulfurasSameQualityAndSellin0() {
		Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", 0, 10) };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertThat(10).isEqualTo(app.items[0].quality);
	}

	@Test
	public void itemNormalDecreasesQuality() {
		Item[] items = new Item[] { new Item("Normal item", 8, 10) };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertThat(9).isEqualTo(app.items[0].quality);
	}

	@Test
	public void itemNormalDecreasesTwiceQuality() {
		Item[] items = new Item[] { new Item("Normal item", 0, 10) };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertThat(8).isEqualTo(app.items[0].quality);
	}

	@Test
	public void itemNormalQualityNotNegative() {
		Item[] items = new Item[] { new Item("Normal item", 5, 0) };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertThat(0).isEqualTo(app.items[0].quality);
	}

	@Test
	public void itemConjuredDecreasesQuality() {
		Item[] items = new Item[] { new Item("Conjured", 8, 10) };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertThat(8).isEqualTo(app.items[0].quality);
	}

	@Test
	public void itemConjuredDecreasesTwiceQuality() {
		Item[] items = new Item[] { new Item("Conjured", 0, 10) };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertThat(6).isEqualTo(app.items[0].quality);
	}

	@Test
	public void itemConjuredQualityNotNegative() {
		Item[] items = new Item[] { new Item("Conjured", 5, 0) };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertThat(0).isEqualTo(app.items[0].quality);
	}

}
